import React, { useEffect } from 'react';
import './App.css';
import MenuButtonComponent from "./shared/Buttons/MenuButtonComponent";
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import { Typography } from '@material-ui/core';
import SearchGame from './games/search-games/SearchGame';
import PopularGames from './games/popular-games/PopularGames';
import AccountCircle from '@material-ui/icons/AccountCircle'
import AppbarComponent from "./shared/AppBar/AppBarComponent";
import NewestGames from './games/newest-games/NewestGames';
import SearchInputFieldComponent from './shared/SearchInputFields/SearchInputFieldComponent';
import ComboboxComponent from './shared/Comboboxes/ComboboxComponent';


function AppData(props){
    const [chosenPlatform, setChosenPlatform] = React.useState(0);
    const [searchString, setSearchString] = React.useState('');
    const [inputEnteredInSearchField, setInputEnteredInSearchField] = React.useState(false);

    // this should be under account button
    const userId = props.location.state.userId;

    function handleNewChosenPlatform(newValue) {
      setChosenPlatform(newValue);
    }
    function handleSearchString(newValue) {
      if (newValue == '') {
        setInputEnteredInSearchField(false);
      }
      else {
        setInputEnteredInSearchField(true);
      }
      setSearchString(newValue);
    }
  
    useEffect(() => {
    }, [chosenPlatform, searchString, userId]);
  
  
    const iconButton = <IconButton><AccountCircle/></IconButton>;
    const data = inputEnteredInSearchField ? <div> <div className="search"> <p>Search Query Results</p></div><SearchGame input={searchString} /> </div> : <div className="data">
      <div className="newest">
        <NewestGames platform={chosenPlatform} userId={userId}/>
      </div>
      <div className="popular">
        <PopularGames platform={chosenPlatform} userId={userId}/>
      </div>
    </div>;

    function handleClick(e){
      alert("test");
    }
    const appBar = <AppbarComponent title={<Typography variant="h6">Gaming Platform</Typography>}
                                    filter={<ComboboxComponent selected={chosenPlatform} onChange={handleNewChosenPlatform} name="Platforms" />}
                                    search={<SearchInputFieldComponent value={searchString} onChange={handleSearchString} icon={<SearchIcon />} />}
                                    account={<MenuButtonComponent userId={userId} onClick={handleClick} button={iconButton} />} />;

     const AppBar =  () => <div className="header">{appBar}</div>
     const Data =  () => <div className="data">{data}</div>

    return(
        <div>
            <div className="header">
                <AppBar/>
            </div>
            <div className="body">
                <Data />
            </div>
        </div>
    );
}
export default AppData;