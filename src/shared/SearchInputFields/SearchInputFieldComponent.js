import { InputBase, makeStyles, fade } from '@material-ui/core';
import React from 'react';


const useStyles = makeStyles((theme) => ({
  search: {
    position: 'relative',
    color: "white",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    color: "black",
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: fade(theme.palette.common.white, 0.45),
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));


function SearchInputFieldComponent(props) {
  const classes = useStyles();
  const WrappedIcon = () => <div>
    <div className={classes.searchIcon}>{props.icon}</div>
  </div>
  const [searchString, setSearchString] = React.useState('');


  function handleChange(event) {
    setSearchString(searchString);
    props.onChange(event.target.value);
  }

  return (
    <div className={classes.search}>
      <WrappedIcon />
      <div>
        <InputBase
          placeholder={"Search…"}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput,
          }}
          type="text"
          autoFocus={true}
          value={props.value}
          onChange={handleChange}
          inputProps={{ 'aria-label': 'search' }} />
      </div>
    </div>
  );
}


export default SearchInputFieldComponent;