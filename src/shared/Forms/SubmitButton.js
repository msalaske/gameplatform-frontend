import React from 'react';
import Button from '@material-ui/core/Button'

function SubmitButton(props) {
    const action = props.action;
    const requestBody = props.body;
    const endpoint = props.endpoint;

    function postDataToBackend(e) {
        fetch(`${process.env.REACT_APP_BACKEND_URI}${endpoint}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody)
        })
            .then((response) => response.json())
            .then((data) => {
                if (data === undefined) alert(`Failed to perform ${action}`);
                else alert("success", handleResponse(data, action));
            })
    }

    // might get to complex at some point 
    function handleResponse(response, action) {
        if (action == "Login") {
            props.handleLoginResponse(response);
        }
        if (action == "Reset Password") {
            props.handleResetPassword(response);
        }
        if(action == "Add Rating"){
            props.handleAddRatingResponse(response);
        }
    }

    return (
        <Button variant="outlined" color="primary" onClick={postDataToBackend}>
            {action}
        </Button>
    );
}
export default SubmitButton;