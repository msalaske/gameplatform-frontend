import { InputBase, makeStyles, fade } from '@material-ui/core';
import React from 'react';


const useStyles = makeStyles((theme) => ({
  inputRoot: {
    color: "black",
    textAlign: "center",
  },
  inputInput: {
    transition: theme.transitions.create('width'),
    width: '20%',
    [theme.breakpoints.up('md')]: {
      width: '50ch',
    },
  },
}));


function FormInputField(props) {
  const classes = useStyles();
  const placeholder = props.placeholder;
  const label = props.label;
  const type = props.type;
  const [value, setValue] = React.useState('');

  function handleChange(event) {
    setValue(value);
    props.onChange(event.target.value);
  }

  return (
    <InputBase
      placeholder={placeholder}
      classes={{
        root: classes.inputRoot,
        input: classes.inputInput,
      }}
      type={type}
      autoFocus={true}
      value={props.value}
      onChange={handleChange}
      inputProps={{ 'aria-label': { label } }} />
  );
}
export default FormInputField;