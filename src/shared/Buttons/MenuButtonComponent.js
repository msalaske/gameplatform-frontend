import React from 'react';
import '../Buttons/MenuButtonComponent.css'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

function MenuButtonComponent(props) {
    const button = props.button;
    const userId = props.userId;
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
        GenerateResetPasswordLink();
    };

    // todo dies vielleicht besser auslagern?
    function GenerateResetPasswordLink(){
        fetch(`${process.env.REACT_APP_BACKEND_URI}users/generateResetPasswordLink`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({userId: userId})
        })
            .then((response) => response.json())
            .then((data) => {
                if (data === undefined) alert(`Failed to generate Mail to reset password.`);
                else alert("You have received an Email to reset your password.");
            })
    }
    return (
        <div>
        <div className="button"  aria-haspopup="true" onClick={handleClick}>
            {button}
        </div>
        <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}>
            <MenuItem onClick={handleClose}>Logged in with userId: {userId}</MenuItem>
            <MenuItem onClick={handleClose}>Reset Password</MenuItem>
            <MenuItem onClick={handleClose}>Logout</MenuItem>
      </Menu>
        </div>       
    );
}
export default MenuButtonComponent;