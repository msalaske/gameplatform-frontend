import React, { useEffect} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import Row from './Row';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

function TableComponent(props) {

    const data = props.data;
    const title = props.title;
    const userId = props.userId;
    console.log(userId);
    return (
        <div>
            <div className={title}>
             <Typography variant="h6">
                             {title}
            </Typography>
            </div>
            <TableContainer component={Paper}>
                <Table stickyHeader aria-label="collapsible table">
                    {data.slice(0, 1).map((entry) => (
                        <TableHead>
                            <TableCell>Details</TableCell>
                            <TableCell>Rate!</TableCell>
                            <TableCell>Subscribe</TableCell>
                            {Object.keys(entry).map((key) => (
                                <TableCell>{key}</TableCell>
                            ))}
                        </TableHead>
                    ))}
                    <TableBody>
                            {data.map((row) => (
                            <Row userId={userId} key={data.key} input={row} />
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}
export default TableComponent;