import React, { useEffect, useState, setError } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import { KeyboardArrowDown, KeyboardArrowUp, Unsubscribe } from '@material-ui/icons';
import GradeIcon from '@material-ui/icons/Grade';
import IconButtonComponent from '../Buttons/MenuButtonComponent';
import Button from '@material-ui/core/Button';
import Auth from '../../auth/Auth';
import { useHistory } from 'react-router-dom';


const useRowStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
});

function Details(gameId) {
    const [details, setDetails] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_BACKEND_URI}games/${gameId}/details`)
            .then((response) => response.json())
            .then((data) => {
                setDetails(data);
            })
            .catch((e) => {
                setError('fetch failed');
            });
    }, []);
    return details;
}

function Subscribe(userId, gameId) {
    fetch(`${process.env.REACT_APP_BACKEND_URI}games/subscribe`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({userId: userId, gameId: gameId})
    })
}

function UnSubscribe(userId, gameId) {
    fetch(`${process.env.REACT_APP_BACKEND_URI}games/unsubscribe`, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({userId: userId, gameId: gameId})
    })
}


function Row(props) {
    const history = useHistory();

    
    const data = props.input;
    const userId = props.userId;
    const [open, setOpen] = useState(false);
    const [subscribed, setSubscribed] = useState(false);
    const classes = useRowStyles();
    
    function redirectToAddRatingForm(){
        const title = data.title;
        const gameId = data.id;
        if(Auth.isAuthenticated()){
            history.push({
                pathname: '/addRating',
                state: { userId: userId, title: title, gameId: gameId}
              });   
        }
    }

    function handleSubscription(){
        const gameId = data.id;
        const userId = props.userId;
        setSubscribed(!subscribed);
        if(subscribed == false){
            Subscribe(userId, gameId);
        }
        else{
            UnSubscribe(userId, gameId);
        }
    }
    return (
        <React.Fragment>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
                    </IconButton>
                </TableCell>
                <TableCell>
                    <IconButton >
                        <GradeIcon onClick={redirectToAddRatingForm}/>
                    </IconButton>
                </TableCell>
                <TableCell>
                        {subscribed ? <Button onClick={() => handleSubscription()} color="secondary">UnSubscribe</Button> : <Button onClick={() => handleSubscription()} color="primary">Subscribe</Button>}
                </TableCell>
                {Object.values(data).map((row) => (
                    <TableCell>{row}</TableCell>
                ))}
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                Details
                    </Typography>
                            <Table stickyHeader size="small" aria-label="details">
                                <TableHead>
                                    <TableRow>
                                        {Object.keys(Details(data.id)).map((value) => (
                                            <TableCell>{value}</TableCell>
                                        ))}
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow >
                                        {Object.values(Details(data.id)).map((value) => (
                                            <TableCell>{value}</TableCell>
                                        ))}
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}
export default Row;