import React, { useState, useEffect, setError } from 'react';
import { makeStyles } from '@material-ui/core';
import FormHelperText from '@material-ui/core/FormHelperText';
import { fade } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    color: fade(theme.palette.common.white, 0.45),
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


function ComboboxComponent(props) {
  const [platforms, setPlatforms] = useState([]);
  const classes = useStyles();
  const selectedItem = props.selected;
  const name = props.name;

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_URI}platforms/`)
      .then((response) => response.json())
      .then((data) => {
        setPlatforms(data);
      })
      .catch((e) => {
        setPlatforms([]);
      });
  }, []);

  function handleChange(event) {
    props.onChange(event.target.value);
  }

  return (
    <div>
      <FormControl className={classes.formControl}>
        <NativeSelect name={name}
                      value={selectedItem}
                      className={classes.selectEmpty}
                      inputProps={{ 'aria-label': { name } }}
                      onChange={handleChange}>
              <option value={0}>None</option>
              {platforms.map((entry) => (
                <option value={entry.id}>{entry.name}</option>
              ))};
        </NativeSelect>
        <FormHelperText color="white">Platforms</FormHelperText>
      </FormControl>
    </div>
  );
}


export default ComboboxComponent;