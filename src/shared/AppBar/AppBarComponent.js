import React from 'react';
import {AppBar, Toolbar, makeStyles} from '@material-ui/core';
 

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    color: 'white',
    height: 100,
    padding: '0px auto',
  },
  menu:{
    marginLeft: theme.spacing(0),
  },
  equalSpacing:{
    marginLeft: theme.spacing(20),
  }
}));

function AppBarComponent(props) {
    const classes = useStyles();
    const WrappedElements =  () => <div className={classes.root}>
                                        <div className={classes.menu}>{props.menu}</div>
                                        <div className={classes.equalSpacing}>{props.title}</div>
                                        <div className={classes.equalSpacing}>{props.filter}</div>
                                        <div className={classes.equalSpacing}>{props.search}</div>
                                        <div className={classes.equalSpacing}>{props.account}</div>
                                   </div>
    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <WrappedElements/>
                </Toolbar>
            </AppBar>
        </div>
    );
}


export default AppBarComponent;