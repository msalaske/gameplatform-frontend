import React from 'react';
import './App.css';
import Registration from './auth/Registration/Registration';
import Login from './auth/Login/Login';
import ResetPassword from './auth/ResetPassword/ResetPassword';
import AppData from './AppData';
import AddRating from './ratings/AddRating/AddRating';
import { BrowserRouter,  Route, Switch } from "react-router-dom";
import RegistrationConfirmation from './auth/RegistrationConfirmation/RegistrationConfirmation';
import ProtectedRoute from './auth/ProtectedRoute';
import DirectionToResetPassword from './auth/ResetPassword/DirectionToResetPassword';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login}/>
        <ProtectedRoute exact path="/data" component={AppData}/>
        <Route exact path="/register" component={Registration}/>
        <ProtectedRoute exact path="/resetPassword" component={ResetPassword}/>
        <ProtectedRoute exact path="/addRating" component={AddRating}/>
        <Route exact path="/confirmRegistration"  component={RegistrationConfirmation}/>
        <Route exact path="/resetPasswordForm"  component={DirectionToResetPassword}/>
      </Switch>
    </BrowserRouter>
  );
}
export default App;
