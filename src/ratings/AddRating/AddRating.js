import React from 'react';
import AddRatingForm from './AddRatingForm';
import '../../shared/Forms/form-styles/CommonFormStyle.css';
import { useHistory } from 'react-router-dom';
import Auth from '../../auth/Auth';

function AddRating(props){
    const history = useHistory();

    const userId = props.location.state.userId;
    const gameId = props.location.state.gameId;
    const title = props.location.state.title;

    
    function handleResponse(response){

        if((Auth.isAuthenticated()) && !(response === undefined)){
            history.push({
                pathname: '/data',
                state: { userId: userId }
              });   
        }
        else{
            alert("Adding the rating was not possible!");
        }
    }

    return(
        <div className="root">
            <AddRatingForm userId={userId} gameId={gameId} title={title} response={handleResponse}/>
        </div>
    );
}
export default AddRating;