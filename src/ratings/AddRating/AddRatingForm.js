import React, {useEffect} from 'react';
import { FormControl } from '@material-ui/core';
import '../../shared/Forms/FormInputField';
import FormInputField from '../../shared/Forms/FormInputField';
import SubmitButton from '../../shared/Forms/SubmitButton';
import '../../shared/Forms/form-styles/CommonFormStyle.css';
import addRatingIcon from '../../shared/Images/rating.png';

function AddRatingForm(props) {
    const gameId = props.gameId;
    const title = props.title;
    const userId = props.userId;
    const [addiction, setAddiction] = React.useState('');
    const [sound, setSound] = React.useState('');
    const [action, setAction] = React.useState('');
    const [graphics, setGraphics] = React.useState('');
    const [subject, setSubject] = React.useState('');
    const [commentMessage, setCommentMessage] = React.useState('');
    const [response, setResponse] = React.useState('');

    
    function handleNewAddiction(newAddiction) {
        setAddiction(newAddiction);
    }

    function handleNewSound(newSound) {
        setSound(newSound);
    }

    function handleNewAction(newAction) {
        setAction(newAction);
    }

    function handleNewGraphics(newGraphics) {
        setGraphics(newGraphics);
    }

    function handleNewSubject(newSubject) {
        setSubject(newSubject);
    }


    function handleNewCommentMessage(newCommentMessage) {
        setCommentMessage(newCommentMessage);
    }

    function handleAddRatingResponse(response){
        setResponse(response);
        props.response(response);
    }

    const body = { Addiction: parseInt(addiction), Sound: parseInt(sound), Action: parseInt(action), Graphics: parseInt(graphics), subject: subject, commentMessage: commentMessage, gameId: parseInt(gameId), userId: userId }
    const min = "1";
    const max = "5";

    return (
        <div className="form">
            <FormControl>
                <div className="image">
                    <img src={addRatingIcon} />
                </div>
                <div className="title">
                    <p> Rating for {title}</p>
                </div>
                <div className="form-content">
                    <div className="inputField">
                        <FormInputField placeholder="Addiction" value={addiction} onChange={handleNewAddiction} min={min} max={max} type="text" label="addiction" />
                    </div>
                    <div className="inputField">
                        <FormInputField placeholder="Sound" value={sound} onChange={handleNewSound} min={min} max={max} type="text" label="sound" />
                    </div>
                    <div className="inputField">
                        <FormInputField placeholder="Action" value={action} onChange={handleNewAction} min={min} max={max} type="text" label="action" />
                    </div>
                    <div className="inputField">
                        <FormInputField placeholder="Graphics" value={graphics} onChange={handleNewGraphics} min={min} max={max} type="text" label="graphics" />
                    </div>
                    <div className="inputField">
                        <FormInputField placeholder="Subject" value={subject} onChange={handleNewSubject} type="text" label="subject" />
                    </div>
                    <div className="inputField">
                        <FormInputField placeholder="CommentMessage" value={commentMessage} onChange={handleNewCommentMessage} type="text" label="CommentMessage" />
                    </div>
                </div>
                <div className="submitButton">
                    <SubmitButton handleAddRatingResponse={handleAddRatingResponse} action="Add Rating" endpoint="ratings" successMessage="Successfully added a rating" body={body} />
                </div>
            </FormControl>
        </div>
    );
}
export default AddRatingForm;