import React, { useEffect, useState} from 'react';
import { Redirect, useHistory } from "react-router-dom";
import Auth from '../../auth/Auth';

function RegistrationConfirmation(props) {
    const history = useHistory();

    const queryString = require('query-string');
    const parsed = queryString.parse(window.location.search);
    const userId = parsed.userId;
    const token = parsed.token;


    const [response, setReponse] = useState({});
    const [responseStatus, setResponseStatus] = useState('');

    useEffect(() => {
        fetch(`${process.env.REACT_APP_BACKEND_URI}users/confirmed?userId=${userId}&token=${token}`)
            .then((response) => response.json())
            .then((data) => {
                setReponse(data);
                setResponseStatus('200');
            })
            .catch((e) => {
                setResponseStatus('500');
            });
    }, []);

    var result;
    if(responseStatus == ''){
        result = 'Registration was not succesful.';
    }
    else{
        Auth.login();
        history.push({
            pathname: '/data',
            state: { userId: response.userId }
          }); 
    }

    return (
        <div>
            {result}
        </div>
    );
}
export default RegistrationConfirmation;