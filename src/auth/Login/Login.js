import React, {useEffect} from 'react';
import LoginForm from './LoginForm';
import '../../shared/Forms/form-styles/CommonFormPosition.css';
import { useHistory } from 'react-router-dom';
import Auth from '../Auth';

function Login(){
    const history = useHistory();

    function handleResponse(response){
        // sign user in if authenticated
        if(!(response.userId === undefined)){
            Auth.login();
        }
        if(Auth.isAuthenticated()){
            history.push({
                pathname: '/data',
                state: { userId: response.userId }
              });   
        }
    }

    return(
        <div className="root">
            <LoginForm response={handleResponse}/>
        </div>
    );
}
export default Login;