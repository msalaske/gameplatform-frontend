import React, {useState} from  'react';
import { FormControl} from '@material-ui/core';
import FormInputField from '../../shared/Forms/FormInputField';
import SubmitButton from '../../shared/Forms/SubmitButton';
import '../../shared/Forms/form-styles/CommonFormStyle.css';
import userManagementIcon from '../../shared/Images/user_management.png';
import { Link } from 'react-router-dom';
import './Login.css';

function LoginForm(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [response, setResponse] = useState('');
    
    function handleNewEmail(newEmail) {
        setEmail(newEmail);
    }

    function handleNewPassword(newPassword) {
        setPassword(newPassword);
    }

    function handleLoginResponse(response){
        setResponse(response);
        props.response(response);
        console.log(response);
    }

    const body = { email: email, password: password }
    return (
        <div>
            <div className="form">
                <FormControl>
                    <div className="image">
                                <img src={userManagementIcon} />
                    </div>
                    <div className="title">
                        <p>User login</p>
                    </div>
                    <div className="formContent">
                        <div className="inputField">
                            <FormInputField placeholder="Email" value={email} onChange={handleNewEmail} type="text" label="email" />
                        </div>
                        <div className="inputField">
                            <FormInputField placeholder="Password" value={password} onChange={handleNewPassword} type="password" label="password" />
                        </div>
                    </div>
                    <div className="redirect">
                        <Link to={'/register'}>No account yet? Please register :).</Link>
                    </div>
                    <div className="submitButton">
                        <SubmitButton handleLoginResponse={handleLoginResponse} action="Login" endpoint="users/login" successMessage="Successfull login" body={body} />
                    </div>
                </FormControl>
            </div>
        </div>
    );
}
export default LoginForm;