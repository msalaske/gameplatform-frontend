import React, {useEffect, useState} from 'react';
import { FormControl } from '@material-ui/core';
import '../../shared/Forms/FormInputField';
import SubmitButton from '../../shared/Forms/SubmitButton';
import '../../shared/Forms/form-styles/CommonFormStyle.css';
import FormInputField from '../../shared/Forms/FormInputField'
import userManagementIcon from '../../shared/Images/user_management.png';


function ResetPasswordForm(props) {
    const token = props.token;
    const userId = props.userId;

    useEffect(() => {
    }, []);

    const [newPasswordConfirmed, setNewPasswordConfirmed] = useState('');
    const [newPassword, setPassword] = useState('');
    const [response, setResponse] = useState('');

    function handleNewPasswordConfirmed(newConfirmedPassword) {
        setNewPasswordConfirmed(newConfirmedPassword);
    }

    function handleNewPassword(newPassword) {
        setPassword(newPassword);
    }

    function handleResetPassword(response){
        setResponse(response);
        props.response(response);
    }

    const body = { userId: userId, token: token, newPassword: newPassword, newPasswordConfirmed: newPasswordConfirmed }
    return (
        <div>
            <div className="form">
                <FormControl>
                    <div className="image">
                        <img src={userManagementIcon} />
                    </div>
                    <div className="title">
                        <p>Reset Password</p>
                    </div>
                    <div className="formContent">
                        <div className="inputField">
                            <FormInputField placeholder="Password" value={newPassword} onChange={handleNewPassword} type="password" label="password" />
                        </div>
                        <div className="inputField">
                            <FormInputField placeholder="ConfirmedPassword" value={newPasswordConfirmed} onChange={handleNewPasswordConfirmed} type="password" label="ConfirmedPassword" />
                        </div>
                    </div>
                    <div className="submitButton">
                        <SubmitButton handleResetPassword={handleResetPassword} action="Reset Password" endpoint="users/resetPassword" successMessage="Successfull reset of password" body={body} />
                    </div>
                </FormControl>
            </div>
        </div>
    );
}
export default ResetPasswordForm;