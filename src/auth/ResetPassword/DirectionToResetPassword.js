import React, { useEffect, useState } from 'react';
import { Redirect, useHistory } from "react-router-dom";
import Auth from '../Auth';

function DirectionToResetPassword(props) {
    const history = useHistory();

    const queryString = require('query-string');
    const uri = window.location.href;
    const parsed = queryString.parse(window.location.search);
    const userId = parsed.userId;
    const token = parsed.token;

    const [result, setResult] = useState('');

    useEffect(() => {
    }, []);

    fetch(`${process.env.REACT_APP_BACKEND_URI}users/checkValidityOfResetPasswordURI`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ URI: uri })
    })
        .then((response) => response.json())
        .then((uri) => {
            if (uri.valid == false) {
                setResult(<div>
                            <h1>This reset password link is invalid!</h1>
                            <p>Either your token is expired or you already have changed password.</p>
                        </div>);
            }
            else {
                // resetPassword only valid for a authenticated user.
                Auth.login();
                history.push({
                    pathname: '/resetPassword',
                    state: { userId: userId, token: token }
                });
            }
        })

    return (
        <div>
            {result}
        </div>
    );
}
export default DirectionToResetPassword;