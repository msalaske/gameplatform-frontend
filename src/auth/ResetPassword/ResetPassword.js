import React, {useEffect} from 'react';
import ResetPasswordForm from './ResetPasswordForm';
import { useHistory } from 'react-router-dom';
import '../../shared/Forms/form-styles/CommonFormPosition.css';
import Auth from '../Auth';

function ResetPassword(props){
    const history = useHistory();

    const userId = props.location.state.userId;
    const token = props.location.state.token;

    useEffect(() => {
    }, []);

    function handleResponse(response){
        if(response === undefined){
            alert("Reset of password was not possible!");
        }
        history.push({
            pathname: '/',
        });   
    }

    return(
        <div className="root">
            <ResetPasswordForm userId={userId} token={token} response={handleResponse}/>
        </div>
    );
}
export default ResetPassword;