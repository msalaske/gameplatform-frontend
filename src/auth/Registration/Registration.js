import React from 'react';
import RegistrationForm from './RegistrationForm';
import '../../shared/Forms/form-styles/CommonFormPosition.css';
function Registration(){
    return(
        <div className="root">
            <RegistrationForm/>
        </div>
    );
}
export default Registration;