import React, {useState} from 'react';
import { FormControl } from '@material-ui/core';
import '../../shared/Forms/FormInputField';
import FormInputField from '../../shared/Forms/FormInputField';
import SubmitButton from '../../shared/Forms/SubmitButton';
import '../../shared/Forms/form-styles/CommonFormStyle.css';
import userManagementIcon from '../../shared/Images/user_management.png';
import { Link } from 'react-router-dom';

function RegistrationForm() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmedPassword, setConfirmedPassword] = useState('');


    function handleNewEmail(newEmail) {
        setEmail(newEmail);
    }

    function handleNewPassword(newPassword) {
        setPassword(newPassword);
    }

    function handleNewConfirmedPassword(newConfirmedPassword) {
        setConfirmedPassword(newConfirmedPassword);
    }

    const body = { userName: email.split('@')[0], email: email, confirmedPassword: confirmedPassword, password: password }

    return (
        <div>
            <div className="form">
                <FormControl>
                    <div className="image">
                        <img src={userManagementIcon} />
                    </div>
                    <div className="title">
                        <p>User Registration</p>
                    </div>
                    <div className="formContent">
                        <div className="inputField">
                            <FormInputField placeholder="Email" value={email} onChange={handleNewEmail} type="text" label="email" />
                        </div>
                        <div className="inputField">
                            <FormInputField placeholder="Password" value={password} onChange={handleNewPassword} type="password" label="password" />
                        </div>
                        <div className="inputField">
                            <FormInputField placeholder="Confirmed Password" value={confirmedPassword} onChange={handleNewConfirmedPassword} type="password" label="confirmedPassword" />
                        </div>
                    </div>
                    <div className="redirect">
                        <Link to={'/'}>Retry Login?</Link>
                    </div>
                    <div className="submitButton">
                        <SubmitButton action="Register" successMessage="Confirm registration via clicking on link in Email" endpoint="users" body={body} />
                    </div>
                </FormControl>
            </div>
        </div>
    );
}
export default RegistrationForm;