import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import Auth from './Auth';

const ProtectedRoute = ({ component: Comp, path, ...rest }) => {
    return (
      <Route
        path={path}
        {...rest}
        render={props => {
          return Auth.isAuthenticated() ? <Comp {...props} /> : <Redirect to="/" />;
        }}
      />
    );
  };
export default ProtectedRoute;
