import React, {useEffect} from 'react';
import AppbarComponent from "./shared/AppBar/AppBarComponent";

function AppBar(props) {
    const title = props.title;
    const filter = props.filter;
    const search = props.search;
    const account = props.account;

    return (
        <div class="AppBar">
            <AppbarComponent 
                title={title}
                filter={filter}
                search={search}
                account={account} />
        </div>
    );
}

export default AppBar;