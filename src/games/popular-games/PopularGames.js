import React, { useEffect, useState} from 'react';
import Table from '../../shared/Table/TableComponent';

function PopularGames(props) {
    const platform = props.platform;
    const [popularGames, setPopularGames] = useState([]);
    const userId = props.userId;
    useEffect(() => {
        fetch(`${process.env.REACT_APP_BACKEND_URI}games/mostpopular?platform=${platform}`)
            .then((response) => response.json())
            .then((data) => {
                setPopularGames(data);
            })
            .catch((e) => {
                setPopularGames([]);
            });
    }, []);

    return (
        <div>
            <Table data={popularGames} title="Popular games" userId={userId}/>
        </div>
    );
}


export default PopularGames;