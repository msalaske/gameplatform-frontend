import React, { useEffect, useState, setError } from 'react';
import Table from '../../shared/Table/TableComponent';

function NewestGames(props) {
    const platform = props.platform;
    const userId = props.userId;

    const [newestGames, setNewestGames] = useState([]);
    useEffect(() => {
        fetch(`${process.env.REACT_APP_BACKEND_URI}games/newest?platform=${platform}`)
            .then((response) => response.json())
            .then((data) => {
                setNewestGames(data);
            })
            .catch((e) => {
                setNewestGames([]);
            });
    }, []);


    return (
        <div>
              <Table data={newestGames} title="Newest Games" userId={userId}/>
        </div>
    );
}


export default NewestGames;