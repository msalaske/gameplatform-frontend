import React, { useState, useEffect} from 'react';
import Table from '../../shared/Table/TableComponent';

function SearchGame(props) {
    const searchString = props.input;
    const [searchResults, setSearchResults] = useState([]);
    useEffect(() => {
        fetch(`${process.env.REACT_APP_BACKEND_URI}games?title=${searchString}`)
            .then((response) => response.json())
            .then((data) => {
                setSearchResults(data);
            })
            .catch((e) => {
                setSearchResults([]);
            });
    }, []);
    return (
        <div>
            <Table data={searchResults} />
        </div>
    );
}

export default SearchGame;